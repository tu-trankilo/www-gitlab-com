---
layout: markdown_page
title: "Category Vision - Source Code Management"
---

- TOC
{:toc}

## Source Code Management

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

Source Code Management provides the core workflows and controls for teams to collaborate using Git to build great software,
including protected branches, code owners, merge request approvals, and mirroring.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=repository)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=repository)
- [Overall Vision](/direction/create/)

Interested in joining the conversation for this category? Please join us in our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/687) where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

## Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->


## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

- **In Progress:** [One-click merge of multiple merge requests](https://gitlab.com/groups/gitlab-org/-/epics/881)

    This is the first iteration of **Cross project merge requests** (née "group merge requests"). Improving cross project workflows is critical to customers with complex project structures that span multiple projects.

- **In Progress:** [Private forks for confidential merge requests](https://gitlab.com/groups/gitlab-org/-/epics/753)

    Public projects need to be able to resolve security issues in private so that the vulnerability isn't leaked during the development process. The MVC is to support workflows where there is a public project, and a private fork.

- **Next:** [Forking improvements](https://gitlab.com/groups/gitlab-org/-/epics/1421)

    Forking workflows are important for open source projects on public instances like GitLab.com, but they are also used for private projects on GitLab.com and elsewhere. There are a range of significant shortcomings in the forking workflow that should be resolved. Forking workflows should be fully supported in GitLab so that they can be used by open source projects and enterprises, public or private.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

## Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the **Loveable** maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [GitHub.com](https://github.com)
- [GitHub Enterprise](https://github.com/enterprise)
- [Bitbucket Cloud](https://bitbucket.org/product/)
- [Bitbucket Server](https://bitbucket.org/product/)
- [Perforce](https://perforce.com)
- [CVS: Concurrent Versions System](https://nongnu.org/cvs/)
- [SVN: Apache Subversion](https://subversion.apache.org/)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Large file support (see [Gitaly direction](/direction/create/gitaly)) is an ongoing area of interest because it blocks certain segments of software development from using Git.

Similarly extremely large repository support (see [Gitaly direction](/direction/create/gitaly)) is also an area of interest for the same reason.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The most frequent category of request is for improved support for scale and team velocity. As companies grow, software projects have a tendency to grow in size and complexity. The applications and repositories tend to be a thorn in the side, for which GitLab can be of assistance.

- Support for very large repositories. Details be viewed in more detail on the [Gitaly direction](/direction/create/gitaly)) page.
- [Coordinate merge requests across multiple projects](https://gitlab.com/groups/gitlab-org/-/epics/881) eases the challenge of making changes to applications with tightly coupled components that are split across multiple projects. (previously called Group Merge Requests)
- [Reduce merge request contention for fast-forward merge](https://gitlab.com/groups/gitlab-org/-/epics/521) is very important to customers who prefer a fast-forward merge strategy and have a non-trivial number of merge requests merging per day. Significant overhead exists needing to rebase and push multiple times before being able to merge.

Improved support for forking workflows, is important for customers large and small, commercial and open source. Bringing the forking workflows up to par with shared repository workflows is very important to these customers.

- [Fast forking](https://gitlab.com/groups/gitlab-org/-/epics/607) is critical for large development teams that have short live forks.
- [Best practice forking](https://gitlab.com/groups/gitlab-org/-/epics/868) - Some organizations/people that use forking have requested automatic mirroring of the upstream project to the downstream fork. This problem is better solved by helping people use forks more efficiently. We can do this with tips and Git output to help people succeed.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Add project setting to set defaults and disable usage of squash merge](https://gitlab.com/gitlab-org/gitlab-ce/issues/49221)
- [Configurable defaults for "Remove source branch" on merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues/32884)
- [Clone statistics](https://gitlab.com/gitlab-org/gitlab-ce/issues/21743)
- [Improved mirroring](https://gitlab.com/groups/gitlab-org/-/epics/852)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/product/#dogfood-everything)
the product.-->

- [Resolve confidential issues using private forks](https://gitlab.com/groups/gitlab-org/-/epics/753) is important for streamlining the security release process. This will also benefit other open source projects too.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Distributed merge requests](https://gitlab.com/groups/gitlab-org/-/epics/260) will overcome the single server boundaries of merge requests, and allow teams on GitLab.com or self-hosted GitLab instances to merge their changes from one server to another.
- Support for monolithic repositories (see [Gitaly direction](/direction/create/gitaly))
