---
layout: markdown_page
title: "Category Vision - Web IDE"
---

- TOC
{:toc}

## Web IDE

### Introduction and how you can help
Thanks for visiting this category page on the Web IDE in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Kai Armstrong([E-Mail](mailto:karmstrong@gitlab.com)).

This vision is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=web%20ide) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=web%20ide) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for the Web IDE, we'd especially love to hear from you.

### Overview
Writing and editing code is essential to the development process, but is almost entirely dependent on local development workflows for all but the most trivial changes. Editing more than a single file using the GitLab interface has historically been laborious because each file must be edited one by one. This makes it harder to resolve feedback in merge requests, fix small bugs when reading source code, or contribute to new projects.

Contributing to a new project has numerous barriers, one of which is setting up a local development environment. In open source projects or private organizations that have large complex development environments it may be challenging to setup a proper environment and require a significant amount of time just to get started.

We want to make it easy for everyone to contribute. Removing barriers will help people contribute more than fixes to typos and dead links, but allow people to contribute larger changes, offer more comprehensive code review suggestions and be able to do meaningful development in a cloud development environment.

A Web IDE (integrated development environment) should support:
 - editing and committing changes to multiple files (beta released [GitLab 10.4](/2018/01/22/gitlab-10-4-released/#web-ide-editor-beta))
 - run unit tests before committing changes
 - previewing and manually testing changes before committing changes

<!-- ### Target Audience and Experience -->
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

### Maturity

Currently, GitLab's maturity in the Web IDE is *viable*. Here's why:

 - GitLab currently offers a rich editing experience via the Web IDE. However, the Web IDE does not currently support common functions for build automation and debugging that would be expected in an IDE. Without these features the Web IDE cannont reasonably replace alternative IDE's.

A **complete** Web IDE category would move beyond a rich editing experience and include the tools users require to write and test their code. Bringing in the features for evaluating code, linting/formatting/completion, and syntax highlighting preferences further the Web IDE's use beyond simple edits to complete development processes. GitLab's WebIDE will become a valuable tool in the developers toolkit as more of their work is moved to GitLab.

A **Lovable** Web IDE comes when we're expanding beyond replacing local development environments to enabling [live coding](https://about.gitlab.com/direction/create/live_coding/) and supporting configurable development environments for users working across multiple language or dependency projects. 

### What's Next & Why
**In progress:** Server side evaluation [&167](https://gitlab.com/groups/gitlab-org/-/epics/167)

The Web IDE is currently a well featured web editor built on Monaco that is integrated with merge requests and CI, but it isn't yet a fully functional integrated development environment.

Using GitLab CI runners we will open a Web Terminal to a CI runner when the Web IDE is opened so that tests can be run in real-time. Changes will be mirrored from the Web IDE to the CI runner. Once changes are mirrored live from the Web IDE to the runner, we should be able to expose a port to the runner and serve contents from the development environment.

**In progress:** Self-hosted client side evaluation [&484](https://gitlab.com/groups/gitlab-org/-/epics/484)

Client-side evaluation live preview in the Web IDE is powered by Codesandbox and currently relies on Codesandbox services to retrieve and package dependencies. Ideally, self hosted GitLab installations should be able to use Codesandbox capabilities in an entirely self hosted manner.

**Next**: Lint, Format and Code Completion [&70](https://gitlab.com/groups/gitlab-org/-/epics/70)

As users continue to move more work to the Web IDE tools commonly found in local environments become more important. By bringing linting, formatters and code completion to the Web IDE users can be more confident in their work and spend less time in code reviews working through simple errors and poor formating.

### Competitive Landscape
- [Cloud9](https://aws.amazon.com/cloud9/)
- [Codesandbox](https://codesandbox.io/)
- [Repl.it](https://repl.it/)
- [Koding](https://www.koding.com/)
- [StackBlitz](https://stackblitz.com/)

### Analyst Landscape
- https://www.g2crowd.com/categories/integrated-development-environment-ide
- https://www.theserverside.com/news/450433105/AWS-Cloud9-IDE-threatens-Microsoft-developer-base


<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ### Top user issue(s) -->
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- Wrong parent when the file changes during editing [gitlab-org/gitlab-ce#59023](https://gitlab.com/gitlab-org/gitlab-ce/issues/59023)

### Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->


