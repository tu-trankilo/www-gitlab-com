---
layout: markdown_page
title: "Board"
---

## Common Links

| **GitLab.com Status** | [**Status Page**](https://status.gitlab.com/) | **Getting help** | [**How to get help**](/handbook/engineering/infrastructure/production/#how-to-get-help) |
| **Incidents** | [Incident response](/handbook/engineering/infrastructure/incident-management/) | **Changes**   | [Managing changes](/handbook/engineering/infrastructure/change-management/) |
| **State** | [**Production Board**](https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1204483) | [**On-Call Board**](https://gitlab.com/groups/gitlab-com/-/boards/962703?&label_name[]=SRE%3AOn-call) | |
| **Issue Trackers** | [Incidents](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident), [Hotspots](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=hotspot)  | [Changes](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=change) | [Deltas](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=delta) |
| **Slack Channels** | [#incident-management](https://gitlab.slack.com/archives/incident-management) | [#alerts](https://gitlab.slack.com/archives/alerts) | [#production](https://gitlab.slack.com/archives/production) |
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) | **On-call** |[Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=oncall%20report) |

## On this page
{:.no_toc}

- TOC
{:toc}

## The Production Board

The [**Production Board**](https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1204483) keeps track of the state of Production, showing, at a glance, incidents, hotspots, changes and deltas related to production, and it also includes on-call reports.

There are four types of issues related to production, denoted by labels:

| Label      | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| `incident` | Incidents are anomalous conditions where GitLab.com is operating below established SLOs. |
| `hotspot`  | Hotspots identify threats that are likely to become incidents if not addressed but that we are unable to address right away. |
| `change`   | Changes are scheduled changes through mainatenance windows.  |
| `delta`    | Deltas reflect devitations from standard configuration that will eventually merge into the standard. |

### Logistics

The Production Board is groomed by the IMOC/CMOC on a daily basis, and we strive to keep it both clean and lean.
